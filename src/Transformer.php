<?php

namespace Kommercio\Api;

use Kommercio\Api\Exceptions\TransformerException;

abstract class Transformer {

    /**
     * @var array
     * Define type to cast a key into. For defining type of list, add `[]` after the key. Eg: `'products[]' => Product::class`
     */
    protected $casts = [];

    /**
     * Transform $properties into object properties
     * @param array|object $properties
     * @throws \Throwable
     */
    public function __construct($properties) {
        if (!is_object($properties) && !is_array($properties)) {
            throw new TransformerException(static::class, $properties);
        }

        foreach ($properties as $key => $property) {
            $type = $this->typeToCast($key);

            $this->$key = $this->transform($property, $type);
        }
    }

    /**
     * @param mixed $property
     * @param null|string $type
     * @return mixed
     * @throws \Throwable
     */
    protected function transform($property, $type = null) {
        try {
            if (is_array($property)) {
                return $this->transformFromList($property, $type);
            }

            if ($type) {
                return $this->cast($property, $type);
            }

            return $property;
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param array $list
     * @param null|string $type
     * @return array
     */
    protected function transformFromList(array $list, $type = null) {
        return array_map(
            function($item) use ($type) {
                return $this->transform($item, $type);
            },
            $list
        );
    }

    /**
     * @param string $propertyName
     * @return null|string
     */
    protected function typeToCast(string $propertyName) {
        return $this->casts[$propertyName]
            ?? ($this->casts[$propertyName . '[]'] ?? null);
    }

    /**
     * @param mixed $value
     * @param string $type
     * @return mixed
     */
    protected function cast($value, $type) {
        if (class_exists($type) && !empty($value)) {
            return new $type($value);
        }

        return $value;
    }
}
