<?php

namespace Kommercio\Api\Exceptions;

use Throwable;

class RequestException extends \RuntimeException {

    /** @var array */
    private $errors;

    /**
     * RequestException constructor.
     * @param string|array $errors
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($errors = "", int $code = 0, Throwable $previous = null) {
        $this->errors = !is_array($errors) ? [
            'general' => [
                $errors
            ],
        ] : $errors;

        parent::__construct($previous ? $previous->getMessage() : 'There is an error in the request', $code, $previous);
    }

    /**
     * @return array
     */
    public function getErrors() {
        return $this->errors;
    }
}
