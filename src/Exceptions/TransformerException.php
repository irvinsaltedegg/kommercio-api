<?php

namespace Kommercio\Api\Exceptions;

use Throwable;

class TransformerException extends \RuntimeException {

    /** @var string */
    private $class;

    /** @var array|object */
    private $properties;

    /**
     * RequestException constructor.
     * @param mixed $class
     * @param array|object $properties
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $class, $properties = [], int $code = 0, Throwable $previous = null) {
        parent::__construct('Unable to transform properties into ' . $class, 0, $previous);
    }

    /**
     * @return string
     */
    public function getClass() {
        return $this->class;
    }

    /**
     * @return array
     */
    public function getProperties() {
        return (array) $this->properties;
    }
}
