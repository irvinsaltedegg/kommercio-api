<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class Page extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $slug;

    /** @var string */
    public $body;

    /** @var string */
    public $metaTitle;

    /** @var string */
    public $metaDescription;

    /** @var array<Image> */
    public $images;

    /** @var array<self> */
    public $children;

    /** @var int */
    public $sortOrder;

    /** @var bool */
    public $active;

    /** @var array */
    protected $casts = [
        'images[]' => Image::class,
        'children[]' => self::class,
    ];
}


