<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Misc\DefaultProduct;
use Kommercio\Api\Transformer;

class ProductComposite extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $label;

    /** @var string */
    public $slug;

    /** @var float */
    public $minimum;

    /** @var float */
    public $maximum;

    /** @var bool */
    public $isFree;

    /** @var int */
    public $sortOrder;

    /** @var array */
    public $productSelections;

    /** @var array */
    public $defaultProducts;

    protected $casts = [
        'productSelections[]' => Product::class,
        'defaultProducts[]' => DefaultProduct::class,
    ];

    public function __construct($properties) {
        parent::__construct($properties);

        // Inject Product model to `defaultProducts`
        $this->defaultProducts = array_map(
            function(DefaultProduct $defaultProduct) {
                $filteredProducts = array_filter(
                    $this->productSelections,
                    function(Product $productSelection) use ($defaultProduct) {
                        return $productSelection->id === $productSelection->id;
                    }
                );

                $product = array_shift($filteredProducts);

                if ($product) $defaultProduct->product = $product;

                return $defaultProduct;
            },
            $this->defaultProducts ?? []
        );
    }
}
