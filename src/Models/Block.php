<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class Block extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $body;

    /** @var string */
    public $machineName;

    /** @var string */
    public $type;

    /** @var bool */
    public $active;
}


