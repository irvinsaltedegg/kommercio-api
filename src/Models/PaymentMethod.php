<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class PaymentMethod extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $class;

    /** @var string */
    public $message;

    /** @var int */
    public $sortOrder;

    /** @var bool */
    public $active;

    /** @var object */
    public $data;
}
