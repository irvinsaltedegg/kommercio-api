<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class Image extends Transformer {

    /** @var string */
    public $filename;

    /** @var string */
    public $caption;

    /** @var object */
    public $crops;

    /** @var string */
    public $locale;

    /**
     * @param string $cropSize
     * @return string
     */
    public function getCropPath(string $cropSize) {
        return $this->crops->{$cropSize} ?? '';
    }
}
