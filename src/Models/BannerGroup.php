<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class BannerGroup extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $slug;

    /** @var string */
    public $body;

    /** @var array<Banner> */
    public $banners;

    /** @var array */
    protected $casts = [
        'banners[]' => Banner::class,
    ];
}


