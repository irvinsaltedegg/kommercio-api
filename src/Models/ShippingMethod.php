<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class ShippingMethod extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $class;

    /** @var string */
    public $message;

    /** @var int */
    public $sortOrder;

    /** @var bool */
    public $taxable;

    /** @var bool */
    public $active;

    /** @var bool */
    public $requireAddress;

    /** @var bool */
    public $requirePostalCode;
}
