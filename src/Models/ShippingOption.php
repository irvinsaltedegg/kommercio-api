<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Misc\Price;
use Kommercio\Api\Transformer;

class ShippingOption extends Transformer {

    /** @var string */
    public $name;

    /** @var string */
    public $description;

    /** @var string */
    public $machineName;

    /** @var Price */
    public $price;

    /** @var ShippingMethod */
    public $shippingMethod;

    /** @var object|null */
    public $extraData;

    protected $casts = [
        'price' => Price::class,
        'shippingMethod' => ShippingMethod::class,
    ];
}

