<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class ProductStock extends Transformer {

    /** @var string */
    public $sku;

    /** @var int */
    public $stock;

    /** @var bool */
    public $manageStock;
}


