<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class Address extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $isoCode;

    /** @var int */
    public $countryCode;

    /** @var bool */
    public $hasDescendant;

    /** @var bool */
    public $customCity;

    /** @var bool */
    public $active;

    /** @var string */
    public $type;

    /** @var int */
    public $sortOrder;
}
