<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class AddressOption extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $type;
}
