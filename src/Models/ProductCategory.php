<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class ProductCategory extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $description;

    /** @var string */
    public $slug;

    /** @var bool */
    public $active;

    /** @var string */
    public $metaTitle;

    /** @var string */
    public $metaDescription;

    /** @var int */
    public $sortOrder;

    /** @var string */
    public $locale;
}
