<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class Profile extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $fullName;

    /** @var string */
    public $phoneNumber;

    /** @var string */
    public $email;

    /** @var string */
    public $address1;

    /** @var string */
    public $address2;

    /** @var string */
    public $customCity;

    /** @var string */
    public $postalCode;

    /** @var Address */
    public $country;

    /** @var Address */
    public $state;

    /** @var Address */
    public $city;

    /** @var Address */
    public $district;

    /** @var Address */
    public $area;

    /** @var array */
    protected $casts = [
        'country' => Address::class,
        'state' => Address::class,
        'city' => Address::class,
        'district' => Address::class,
        'area' => Address::class,
    ];
}


