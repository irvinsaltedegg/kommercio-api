<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class PostCategory extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $body;

    /** @var self */
    public $parent;

    /** @var self[] */
    public $children;

    /** @var string */
    public $slug;

    /** @var string */
    public $metaTitle;

    /** @var string */
    public $metaDescription;

    /** @var array<Image> */
    public $images;

    /** @var int */
    public $sortOrder;

    /** @var string */
    public $locale;

    /** @var array */
    protected $casts = [
        'parent' => self::class,
        'images[]' => Image::class,
        'children[]' => self::class,
    ];
}
