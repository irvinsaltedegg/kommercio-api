<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class MenuItem extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $menuClass;

    /** @var string */
    public $url;

    /** @var string */
    public $urlAlias;

    /** @var int */
    public $sortOrder;

    /** @var bool */
    public $active;

    /** @var Array<self> */
    public $children;

    /** @var string */
    public $urlTarget;

    protected $casts = [
        'children[]' => self::class,
    ];
}
