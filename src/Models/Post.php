<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class Post extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $slug;

    /** @var string */
    public $teaser;

    /** @var string */
    public $body;

    /** @var string */
    public $metaTitle;

    /** @var string */
    public $metaDescription;

    /** @var \DateTime */
    public $createdAt;

    /** @var \DateTime */
    public $updatedAt;

    /** @var array<PostCategory> */
    public $categories;

    /** @var Image */
    public $thumbnail;

    /** @var array<Image> */
    public $images;

    /** @var int */
    public $sortOrder;

    /** @var bool */
    public $active;

    /** @var string */
    public $locale;

    /** @var array */
    protected $casts = [
        'categories[]' => PostCategory::class,
        'images[]' => Image::class,
        'thumbnail' => Image::class,
        'createdAt' => \DateTime::class,
        'updatedAt' => \DateTime::class,
    ];
}


