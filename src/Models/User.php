<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class User extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $email;

    /** @var string */
    public $status;

    /** @var Customer */
    public $customer;

    protected $casts = [
        'customer' => Customer::class,
    ];
}
