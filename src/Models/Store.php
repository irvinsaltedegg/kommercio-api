<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Misc\Price;
use Kommercio\Api\Transformer;

class Store extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $code;

    /** @var string */
    public $type;

    /** @var bool */
    public $isDefault;

    /** @var Address */
    public $country;

    protected $casts = [
        'country' => Address::class,
    ];
}


