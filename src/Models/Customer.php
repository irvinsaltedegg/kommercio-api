<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class Customer extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $email;

    /** @var string */
    public $salute;

    /** @var string */
    public $fullName;

    /** @var string */
    public $phoneNumber;

    /** @var string */
    public $homePhone;

    /** @var \DateTime */
    public $birthday;

    /** @var User */
    public $user;

    protected $casts = [
        'user' => User::class,
        'birthday' => \DateTime::class,
    ];
}
