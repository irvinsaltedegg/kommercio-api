<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class Menu extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $slug;

    /** @var string */
    public $description;

    /** @var Array<MenuItem> */
    public $menuItems;

    protected $casts = [
        'menuItems[]' => MenuItem::class,
    ];
}


