<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;

class OrderLimit extends Transformer {

    /** @var int */
    public $id;

    /** @var \DateTime */
    public $dateFrom;

    /** @var \DateTime */
    public $dateTo;

    /** @var string */
    public $type;

    /** @var string */
    public $limitType;

    /** @var float */
    public $limit;

    /** @var int */
    public $sortOrder;

    /** @var bool */
    public $active;

    /** @var bool */
    public $backoffice;

    /** @var Store */
    public $store;

    /** @var array */
    protected $casts = [
        'store' => Store::class,
    ];
}
