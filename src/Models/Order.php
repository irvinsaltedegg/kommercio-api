<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Misc\Amount;
use Kommercio\Api\Transformer;

class Order extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $reference;

    /** @var string */
    public $publicId;

    /** @var string */
    public $status;

    /** @var \DateTime */
    public $deliveryDate;

    /** @var \DateTime */
    public $checkoutAt;

    /** @var Profile */
    public $shippingProfile;

    /** @var Profile */
    public $billingProfile;

    /** @var float */
    public $quantity;

    /** @var Amount */
    public $total;

    /** @var Store */
    public $store;

    /** @var ShippingMethod */
    public $shippingMethod;

    /** @var string */
    public $shippingOption;

    /** @var PaymentMethod */
    public $paymentMethod;

    /** @var string */
    public $notes;

    /** @var string */
    public $purchaseReason;

    protected $casts = [
        'store' => Store::class,
        'deliveryDate' => \DateTime::class,
        'checkoutAt' => \DateTime::class,
        'total' => Amount::class,
        'shippingMethod' => ShippingMethod::class,
        'paymentMethod' => PaymentMethod::class,
        'shippingProfile' => Profile::class,
        'billingProfile' => Profile::class,
    ];
}
