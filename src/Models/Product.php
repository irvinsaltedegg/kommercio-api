<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Misc\Dimensions;
use Kommercio\Api\Misc\Price;
use Kommercio\Api\Transformer;

class Product extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $sku;

    /** @var string */
    public $type;

    /** @var string */
    public $combinationType;

    /** @var string */
    public $name;

    /** @var string */
    public $slug;

    /** @var string */
    public $descriptionShort;

    /** @var string */
    public $description;

    /** @var string */
    public $metaTitle;

    /** @var string */
    public $metaDescription;

    /** @var string */
    public $locale;

    /** @var array<Image> */
    public $thumbnails;

    /** @var array<Image> */
    public $images;

    /** @var bool */
    public $new;

    /** @var bool */
    public $available;

    /** @var bool */
    public $active;

    /** @var string */
    public $visibility;

    /** @var string */
    public $currency;

    /** @var bool */
    public $taxable;

    /** @var ProductCategory */
    public $defaultCategory;

    /** @var array<ProductCategory> */
    public $categories;

    /** @var array */
    protected $casts = [
        'defaultCategory' => ProductCategory::class,
        'categories[]' => ProductCategory::class,
        'composites[]' => ProductComposite::class,
        'thumbnails[]' => Image::class,
        'images[]' => Image::class,
        'price' => Price::class,
        'dimensions' => Dimensions::class,
    ];

    /**
     * @param int $position
     * @return Image|null
     */
    public function getThumbnail($position = 0) {
        return $this->thumbnails[$position] ?? null;
    }
}
