<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Misc\Amount;
use Kommercio\Api\Transformer;

class LineItem extends Transformer {

    /** @var int */
    public $id;

    /** @var int */
    public $lineItemId;

    /** @var string */
    public $lineItemType;

    /** @var Product */
    public $product;

    /** @var string */
    public $name;

    /** @var Amount */
    public $basePrice;

    /** @var Amount */
    public $netPrice;

    /** @var Amount */
    public $discountTotal;

    /** @var Amount */
    public $taxTotal;

    /** @var Amount */
    public $total;

    /** @var float */
    public $quantity;

    /** @var bool */
    public $taxable;

    /** @var int */
    public $sortOrder;

    protected $casts = [
        'product' => Product::class,
        'basePrice' => Amount::class,
        'netPrice' => Amount::class,
        'discountTotal' => Amount::class,
        'taxTotal' => Amount::class,
        'total' => Amount::class,
    ];
}


