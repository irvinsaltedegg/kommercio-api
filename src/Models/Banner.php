<?php

namespace Kommercio\Api\Models;

use Kommercio\Api\Transformer;
use Kommercio\Api\Misc\Link;

class Banner extends Transformer {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $body;

    /** @var array<Image> */
    public $images;

    /** @var Link */
    public $link;

    /** @var string */
    public $cssClass;

    /** @var bool */
    public $active;

    /** @var int */
    public $sortOrder;

    /** @var array */
    protected $casts = [
        'link' => Link::class,
        'images[]' => Image::class,
    ];

    /**
     * @param int $index
     * @return Image|null
     */
    public function getImage($index = 0) {
        $image = $this->images[$index] ?? null;

        return $image;
    }
}


