<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;
use Kommercio\Api\Models\Store;

class StoreService extends Service {

    /**
     * @return object
     * @throws RequestException
     */
    public function getStores($query = []) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/',
                    [
                        'query' => $query,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($storeData) {
                    return new Store($storeData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/store';
    }
}
