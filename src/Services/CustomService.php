<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;

class CustomService extends Service {

    /**
     * @param string|int $slugOrId
     * @return object
     * @throws RequestException
     */
    public function call($path, $method, $params) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    $method,
                    $this->getPath() . '/' . ltrim($path, '/'),
                    $params
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return $this->emptyDataResponse();
        }
    }

    public function get($path, $queryParams = [], $params = []) {
        return $this->call(
            $path,
            'GET',
            array_replace(
                $params,
                [
                    'query' => $queryParams,
                ]
            )
        );
    }

    public function postAsJSON($path, $body = [], $params = []) {
        return $this->call(
            $path,
            'POST',
            array_replace(
                $params,
                [
                    'json' => $body,
                    'headers' => [
                        'Accept' => 'application/json',
                    ],
                ]
            )
        );
    }

    public function postAsFormData($path, $body = [], $params = []) {
        return $this->call(
            $path,
            'POST',
            array_replace(
                $params,
                [
                    'multipart' => $body,
                ]
            )
        );
    }

    /**
     * Stub
     * @return string
     */
    public function getPath(): string {
        return '';
    }
}
