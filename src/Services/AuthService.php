<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;

class AuthService extends Service {

    /**
     * @param string $email
     * @param array $additionalParams
     * @return object
     * @throws RequestException
     */
    public function forgetPassword(string $email, array $additionalParams = []) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'POST',
                    $this->getPath() . '/forget-password',
                    [
                        'json' => array_merge(
                            $additionalParams,
                            [
                                'email' => $email,
                            ]
                        ),
                        'headers' => [
                            'Accept' => 'application/json',
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());
            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @param string $email
     * @param string $password
     * @param string $token
     * @return object
     * @throws RequestException
     */
    public function resetPassword(string $email, string $password, string $token) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'POST',
                    $this->getPath() . '/reset-password',
                    [
                        'json' => array_merge(
                            [
                                'email' => $email,
                                'password' => $password,
                                'token' => $token,
                            ]
                        ),
                        'headers' => [
                            'Accept' => 'application/json',
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());
            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/auth';
    }
}
