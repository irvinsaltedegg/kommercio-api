<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;
use Kommercio\Api\Models\Page;

class PageService extends Service {

    /**
     * @param string|int $slugOrId
     * @return object|null
     * @throws RequestException
     */
    public function getPage($slugOrId) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/',
                    [
                        'query' => [
                            'slugOrId' => $slugOrId,
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = new Page($jsonResponse->data);

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return $this->emptyDataResponse();
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/page';
    }
}
