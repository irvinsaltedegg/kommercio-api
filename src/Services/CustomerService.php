<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;
use Kommercio\Api\Models\Customer;

class CustomerService extends Service {

    /**
     * @param array $customerData
     * @param array $accountData
     * @return object
     * @throws RequestException
     */
    public function create(array $customerData, array $accountData = []) {
        // If $accountData is given, user associated with a customer will be created along
        if (!empty($accountData)) {
            $accountData = [
                '_create_account' => true,
                'user' => $accountData,
            ];
        }

        try {
            $response = $this
                ->getClient()
                ->request(
                    'POST',
                    $this->getPath() . '/create',
                    [
                        'json' => array_merge(
                            $customerData,
                            $accountData
                        ),
                        'headers' => [
                            'Accept' => 'application/json',
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = new Customer($jsonResponse->data);

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return null;
        }
    }

    /**
     * @param int $customerId
     * @param array $customerData
     * @param array $accountData
     * @return object
     * @throws RequestException
     */
    public function update(int $customerId, array $customerData, array $accountData = []) {
        // If $accountData is given, user associated with a customer will be created along
        if (!empty($accountData)) {
            $accountData = [
                'user' => $accountData,
            ];
        }

        try {
            $response = $this
                ->getClient()
                ->request(
                    'POST',
                    $this->getPath() . '/update/' . $customerId,
                    [
                        'json' => array_merge(
                            $customerData,
                            $accountData
                        ),
                        'headers' => [
                            'Accept' => 'application/json',
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = new Customer($jsonResponse->data);

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return null;
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/customer';
    }
}
