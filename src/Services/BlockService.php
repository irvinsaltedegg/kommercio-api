<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;
use Kommercio\Api\Models\Block;

class BlockService extends Service {

    /**
     * @param string|int $slugOrId
     * @return object|null
     * @throws RequestException
     */
    public function getBlock($slugOrId) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/',
                    [
                        'query' => [
                            'slugOrId' => $slugOrId,
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = new Block($jsonResponse->data);

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return $this->emptyDataResponse();
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/block';
    }
}
