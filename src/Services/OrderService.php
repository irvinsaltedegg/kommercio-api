<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;
use Kommercio\Api\Exceptions\TransformerException;
use Kommercio\Api\Models\Order;
use Kommercio\Api\Models\OrderLimit;
use Kommercio\Api\Models\PaymentMethod;
use Kommercio\Api\Models\ShippingOption;

class OrderService extends Service {

    /**
     * @param array $query
     * @param int $page
     * @param int $perPage
     * @return object
     * @throws RequestException
     */
    public function getOrders($query = [], $page = 1, $perPage = 25) {
        $queryParameters = array_merge(
            $query,
            [
                'page' => $page,
                'per_page' => $perPage,
            ]
        );

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($orderData) {
                    return new Order($orderData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return $this->emptyDataResponse();
        }
    }

    /**
     * @param string $publicId
     * @return object|null
     * @throws RequestException
     */
    public function getOrderByPublicId($publicId) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/get-by-public-id/' . $publicId
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = new Order($jsonResponse->data);

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return $this->emptyDataResponse();
        }
    }

    /**
     * @param array $data
     * @return object
     * @throws RequestException
     */
    public function submit(array $data) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'POST',
                    $this->getPath() . '/submit',
                    [
                        'json' => $data,
                        'headers' => [
                            'Accept' => 'application/json',
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());
            $jsonResponse->data = new Order($jsonResponse->data);

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return $this->emptyDataResponse();
        }
    }

    /**
     * @param array $query
     * @return object
     * @throws RequestException
     */
    public function getShippingOptions($query = []) {
        $queryParameters = $query;

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/shipping-methods',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($shippingOptionData) {
                    return new ShippingOption($shippingOptionData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @param array $query
     * @return object
     * @throws RequestException
     */
    public function getPaymentMethods($query = []) {
        $queryParameters = $query;

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/payment-methods',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($paymentMethodData) {
                    return new PaymentMethod($paymentMethodData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @param array $query
     * @return object
     * @throws RequestException
     */
    public function getAvailability($query = []) {
        $queryParameters = $query;

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/availability',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @param array $query
     * @return object
     * @throws RequestException
     */
    public function getDaysAvailability($query = []) {
        $queryParameters = $query;

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/days-availability',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @param array $query
     * @return object
     * @throws RequestException
     * @throws \Throwable
     */
    public function getPerOrderLimit($query = []) {
        $queryParameters = $query;

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/limit',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            if (!empty($jsonResponse->data)) {
                try {
                    $jsonResponse->data = new OrderLimit($jsonResponse->data);
                } catch (\Throwable $e) {
                    throw $e;
                }
            }

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/order';
    }
}
