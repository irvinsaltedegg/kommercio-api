<?php

namespace Kommercio\Api\Services;

use Kommercio\Api\Exceptions\RequestException;

class NewsletterService extends Service {

    /**
     * @param string $email
     * @param string $group
     * @return boolean|null
     * @throws RequestException
     */
    public function subscribe($email, $group = null) {
        try {
            $this
                ->getClient()
                ->request(
                    'POST',
                    $this->getPath() . '/subscribe',
                    [
                        'json' => [
                            'group' => $group,
                            'email' => $email,
                        ],
                    ]
                );

            return true;
        } catch (\Throwable $e) {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/newsletter';
    }
}
