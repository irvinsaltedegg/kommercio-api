<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;
use Kommercio\Api\Models\Post;
use Kommercio\Api\Models\PostCategory;

class PostService extends Service {

    /**
     * @param array $query
     * @param int $page
     * @param int $perPage
     * @return object
     * @throws RequestException
     */
    public function getPosts($query = [], $page = 1, $perPage = 25) {
        $queryParameters = array_merge(
            $query,
            [
                'page' => $page,
                'per_page' => $perPage,
            ]
        );

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/posts',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($postData) {
                    return new Post($postData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @param string|int $slugOrId
     * @return object|null
     * @throws RequestException
     */
    public function getPost($slugOrId) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/post',
                    [
                        'query' => [
                            'slugOrId' => $slugOrId,
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = new Post($jsonResponse->data);

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return $this->emptyDataResponse();
        }
    }

    /**
     * @param array $query
     * @param int $page
     * @param int $perPage
     * @return object
     * @throws RequestException
     */
    public function getPostCategories($query = [], $page = 1, $perPage = 25) {
        $queryParameters = array_merge(
            $query,
            [
                'page' => $page,
                'per_page' => $perPage,
            ]
        );

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/categories',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($postCategoryData) {
                    return new PostCategory($postCategoryData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @param string|int $slugOrId
     * @return object|null
     * @throws RequestException
     */
    public function getPostCategory($slugOrId) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/category',
                    [
                        'query' => [
                            'slugOrId' => $slugOrId,
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = new PostCategory($jsonResponse->data);

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return $this->emptyDataResponse();
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/post';
    }
}
