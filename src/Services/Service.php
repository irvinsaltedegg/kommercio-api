<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;

abstract class Service implements IService {
    /** @var string */
    private $apiHost;

    /** @var string */
    private $apiKey;

    /** @var array */
    private $headers;

    /** @var Client */
    private $client;

    /**
     * @param string $apiHost
     */
    public function setApiHost(string $apiHost) {
        $this->apiHost = $apiHost;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey(string $apiKey) {
        $this->apiKey = $apiKey;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers) {
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    protected function getHeaders() {
        $headers = $this->headers ?? [];

        /*
        $allowedHeaders = [
            'user-agent' => true,
            'host' => true,
            'origin' => true,
            'x-forwarded-for' => true,
            'x-forwarded-host' => true,
            'content-type' => true,
            'referer' => true,
            'via' => true,
        ];
        */
        $allowedHeaders = [];

        $whitelistedHeaders = [];
        foreach ($headers as $key => $header) {
            if (empty($allowedHeaders) || isset($allowedHeaders[strtolower($key)]) && $allowedHeaders[strtolower($key)]) {
                $whitelistedHeaders[$key] = $header;
            }
        }

        return $whitelistedHeaders;
    }

    public function getRequestUrl() {
        return $this->apiHost . '/' . $this->getPath();
    }

    /**
     * @return object
     */
    public function emptyDataResponse() {
        return (object) [
            'data' => null,
        ];
    }

    protected function getResponseException(GuzzleRequestException $e) {
        $response = $e->getResponse();

        if ($response) {
            $jsonResponse = json_decode(
                $response->getBody()->getContents(),
                TRUE
            );

            if (!isset($jsonResponse['errors'])) {
                $jsonResponse['errors'] = $response->getReasonPhrase();
            }
        } else {
            $jsonResponse['errors'] = $e->getMessage();
        }

        return new RequestException($jsonResponse['errors'], $e->getCode(), $e);
    }

    /**
     * @return Client
     */
    protected function getClient() {
        if (!isset($this->client)) {
            $this->client = $this->buildClient();
        }

        return $this->client;
    }

    protected function buildClient() {
        $options = [
            'base_uri' => $this->apiHost,
            'cookies' => TRUE,
        ];

        $headers = $this->getHeaders();
        if (!empty($headers)) {
            $options['header'] = $headers;
        }

        if (!empty($this->apiKey)) {
            $options = array_merge_recursive(
                $options,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->apiKey,
                    ],
                ]
            );
        }

        return new Client($options);
    }
}
