<?php

namespace Kommercio\Api\Services;

interface IService {
    /**
     * @return string
     */
    function getPath(): string;
}
