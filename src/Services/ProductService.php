<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;
use Kommercio\Api\Models\Product;
use Kommercio\Api\Models\ProductStock;

class ProductService extends Service {

    /**
     * @param array $query
     * @param int $page
     * @param int $perPage
     * @return object
     * @throws RequestException
     */
    public function getProducts($query = [], $page = 1, $perPage = 25) {
        $queryParameters = array_merge(
            $query,
            [
                'page' => $page,
                'per_page' => $perPage,
            ]
        );

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/products',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($productData) {
                    return new Product($productData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @param array $query
     * @return Object
     * @throws RequestException
     */
    public function getStocks($query = []) {
        $queryParameters = $query;

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/stocks',
                    [
                        'query' => $queryParameters,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($stockData) {
                    return new ProductStock($stockData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/product';
    }
}
