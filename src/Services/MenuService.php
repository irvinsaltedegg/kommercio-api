<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;
use Kommercio\Api\Models\Menu;

class MenuService extends Service {

    /**
     * @param string|int $slugOrId
     * @return object
     * @throws RequestException
     */
    public function getMenu($slugOrId) {
        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/',
                    [
                        'query' => [
                            'slugOrId' => $slugOrId,
                        ],
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = new Menu($jsonResponse->data);

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        } catch (\Throwable $e) {
            return $this->emptyDataResponse();
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/menu';
    }
}
