<?php

namespace Kommercio\Api\Services;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

use Kommercio\Api\Exceptions\RequestException;
use Kommercio\Api\Models\Address;
use Kommercio\Api\Models\AddressOption;

class AddressService extends Service {

    /**
     * @param string $type
     * @param int|null $parent
     * @param array $options
     * @return object
     * @throws RequestException
     */
    public function get(string $type, $parent = null, $options = []) {
        $params = [];

        if ($parent) $params['parent'] = $parent;
        if (!empty($options['activeOnly'])) $params['active_only'] = true;

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/' . $type,
                    [
                        'query' => $params,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($addressData) use ($type) {
                    $addressData->type = $type;

                    return new Address($addressData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @param string $type
     * @param int|null $parent
     * @param array $options
     * @return object
     * @throws RequestException
     */
    public function getOptions(string $type, $parent = null, $options = []) {
        $params = [];

        if ($parent) $params['parent'] = $parent;
        if (!empty($options['activeOnly'])) $params['active_only'] = true;
        if (!empty($options['firstOption'])) $params['first_option'] = true;

        try {
            $response = $this
                ->getClient()
                ->request(
                    'GET',
                    $this->getPath() . '/' . $type . '/options',
                    [
                        'query' => $params,
                    ]
                );

            $jsonResponse = json_decode($response->getBody()->getContents());

            $jsonResponse->data = array_map(
                function($addressData) use ($type) {
                    $addressData->type = $type;

                    return new AddressOption($addressData);
                },
                $jsonResponse->data
            );

            return $jsonResponse;
        } catch (GuzzleRequestException $e) {
            throw $this->getResponseException($e);
        }
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return 'api/public/address';
    }
}
