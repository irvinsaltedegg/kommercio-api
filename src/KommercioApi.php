<?php

namespace Kommercio\Api;

use Kommercio\Api\Services\AddressService;
use Kommercio\Api\Services\AuthService;
use Kommercio\Api\Services\BannerService;
use Kommercio\Api\Services\BlockService;
use Kommercio\Api\Services\CustomerService;
use Kommercio\Api\Services\CustomService;
use Kommercio\Api\Services\MenuService;
use Kommercio\Api\Services\NewsletterService;
use Kommercio\Api\Services\OrderService;
use Kommercio\Api\Services\PageService;
use Kommercio\Api\Services\PostService;
use Kommercio\Api\Services\ProductService;
use Kommercio\Api\Services\Service;
use Kommercio\Api\Services\StoreService;

class KommercioApi {
    /** @var string */
    private $apiHost;

    /** @var string */
    private $apiKey;

    /** @var array */
    public $services;

    /**
     * @param string $apiHost
     * @param string $apiKey
     * @param string $options
     */
    public function __construct(string $apiHost, string $apiKey, array $options = []) {
        $this->apiHost = $apiHost;
        $this->apiKey = $apiKey;

        $this->buildServices($options);
    }

    /**
     * @return ProductService
     */
    public function product(): ProductService {
        return $this->services['product'];
    }

    /**
     * @return OrderService
     */
    public function order(): OrderService {
        return $this->services['order'];
    }

    /**
     * @return AuthService
     */
    public function auth(): AuthService {
        return $this->services['auth'];
    }

    /**
     * @return PageService
     */
    public function page(): PageService {
        return $this->services['page'];
    }

    /**
     * @return AddressService
     */
    public function address(): AddressService {
        return $this->services['address'];
    }

    /**
     * @return StoreService
     */
    public function store(): StoreService {
        return $this->services['store'];
    }

    /**
     * @return CustomerService
     */
    public function customer(): CustomerService {
        return $this->services['customer'];
    }

    /**
     * @return MenuService
     */
    public function menu(): MenuService {
        return $this->services['menu'];
    }

    /**
     * @return BlockService
     */
    public function block(): BlockService {
        return $this->services['block'];
    }

    /**
     * @return BannerService
     */
    public function banner(): BannerService {
        return $this->services['banner'];
    }

    /**
     * @return PostService
     */
    public function post(): PostService {
        return $this->services['post'];
    }

    /**
     * @return NewsletterService
     */
    public function newsletter(): NewsletterService {
        return $this->services['newsletter'];
    }

    /**
     * @return CustomService
     */
    public function custom(): CustomService {
        return $this->services['custom'];
    }

    /**
     * @param array $options
     */
    protected function buildServices(array $options) {
        $plannedServices = [
            'product' => ProductService::class,
            'order' => OrderService::class,
            'store' => StoreService::class,
            'address' => AddressService::class,
            'page' => PageService::class,
            'customer' => CustomerService::class,
            'auth' => AuthService::class,
            'menu' => MenuService::class,
            'block' => BlockService::class,
            'banner' => BannerService::class,
            'newsletter' => NewsletterService::class,
            'post' => PostService::class,
            'custom' => CustomService::class,
        ];

        foreach ($plannedServices as $serviceName => $plannedService) {
            if (!isset($this->services[$serviceName])) {
                $this->services[$serviceName] = new $plannedService();
            }
        }

        $headers = $options['headers'] ?? [];
        foreach ($this->services as $service) {
            /** @var Service $service */
            $service->setApiHost($this->apiHost);
            $service->setApiKey($this->apiKey);
            $service->setHeaders($headers);
        }
    }
}
