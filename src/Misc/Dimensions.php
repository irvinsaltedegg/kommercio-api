<?php

namespace Kommercio\Api\Misc;

use Kommercio\Api\Transformer;

class Dimensions extends Transformer {

    /** @var float */
    public $width;

    /** @var float */
    public $height;

    /** @var float */
    public $depth;

    /** @var float */
    public $weight;
}
