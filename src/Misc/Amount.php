<?php

namespace Kommercio\Api\Misc;

use Kommercio\Api\Transformer;

class Amount extends Transformer {

    /** @var float */
    public $amount;

    /**
     * @var Currency
     */
    public $currency;

    /** @var array */
    protected $casts = [
        'currency' => Currency::class,
    ];

    /**
     * @return string
     */
    public function printTotal() {
        $price = $this->amount;

        return $this->currency->formatPrice($price);
    }
}
