<?php

namespace Kommercio\Api\Misc;

use Kommercio\Api\Transformer;

class Price extends Transformer {

    /** @var float */
    public $retailPrice;

    /** @var float */
    public $retailPriceWithTax;

    /** @var float */
    public $netPrice;

    /** @var float */
    public $netPriceWithTax;

    /**
     * @var Currency
     */
    public $currency;

    /** @var array */
    protected $casts = [
        'currency' => Currency::class,
    ];

    /**
     * @param bool $includeTax
     * @return string
     */
    public function printNetPrice(bool $includeTax = false) {
        $price = $this->netPrice;

        if ($includeTax) {
            $price = $this->netPriceWithTax;
        }

        return $this->currency->formatPrice($price);
    }

    /**
     * @param bool $includeTax
     * @return string
     */
    public function printRetailPrice(bool $includeTax = false) {
        $price = $this->retailPrice;

        if ($includeTax) {
            $price = $this->retailPriceWithTax;
        }

        return $this->currency->formatPrice($price);
    }
}

