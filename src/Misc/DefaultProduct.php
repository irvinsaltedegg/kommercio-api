<?php

namespace Kommercio\Api\Misc;

use Kommercio\Api\Models\Product;
use Kommercio\Api\Transformer;

class DefaultProduct extends Transformer {

    /** @var int */
    public $id;

    /** @var int */
    public $sortOrder;

    /** @var float */
    public $quantity;

    /** @var Product */
    public $product;

    protected $casts = [
        'product' => Product::class,
    ];
}

