<?php

namespace Kommercio\Api\Misc;

use Kommercio\Api\Transformer;

class Link extends Transformer {

    /** @var string */
    public $url;

    /** @var string */
    public $target;

    /** @var string */
    public $text;
}
