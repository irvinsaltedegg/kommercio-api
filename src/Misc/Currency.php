<?php

namespace Kommercio\Api\Misc;

use Kommercio\Api\Transformer;

class Currency extends Transformer {

    /** @var string */
    public $symbol;

    /** @var string */
    public $iso;

    /** @var string */
    public $thousandSeparator;

    /** @var string */
    public $decimalSeparator;

    /**
     * @param float $amount
     * @return string
     */
    public function formatPrice(float $amount) {
        $parts = [
            $this->symbol,
            ' ',
            str_replace(
                $this->decimalSeparator . '00',
                '',
                number_format(
                    $amount,
                    2,
                    $this->decimalSeparator,
                    $this->thousandSeparator
                )
            )
        ];
        return implode('', $parts);
    }
}
